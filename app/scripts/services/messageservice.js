'use strict';
angular.module('firebaseApp')
  .service('MessageService', function (config, $q, $firebaseArray) {
    var messagesRef = new Firebase(config.MSGFBURL);
    var fireMessages = $firebaseArray(messagesRef);
    var totalPage = 1;
    var currentPage = 1;
    var itemCount = 0;

    //paging handling
    var navRef = new Firebase(config.MSGFBURL).startAt().limitToFirst(config.pageSize);
    var displayMessages = $firebaseArray(navRef);

    fireMessages.$loaded().then(function () {
      itemCount = fireMessages.length;
      totalPage = Math.ceil(itemCount / config.pageSize);
    });

    var getFbMessages = function(){
      return fireMessages;
    };

    var getFirstPageMessages = function () {
      //get first messages on page
      return displayMessages;
    };


    var add = function addMessage(message) {
      var deferred = $q.defer();
      fireMessages.$add(message);
      //update item  & page
      itemCount++;
      totalPage = Math.ceil(itemCount / config.pageSize);

      deferred.resolve(message);
      return deferred.promise;
    };

    var remove = function removeMessage(message) {
      var msg = fireMessages.$getRecord(message.$id);
      fireMessages.$remove(msg);
      itemCount--;
      var curCount =  displayMessages.length;
      if(curCount <= 1){
          totalPage--;
        return previousPage();
      } else{
        return displayMessages;
      }
    };

    var update = function (message) {
      var msg = fireMessages.$getRecord(message.$id);
      msg.user = message.user;
      msg.text = message.text;
      fireMessages.$save(msg);
    };

    var getMessage = function (key) {
      return fireMessages.$getRecord(key);
    };

    var isFirstPage = function isFirstPage() {
      return currentPage === 1;
    };

    var isLastPage = function isLastPage() {
      return currentPage === totalPage;
    };

    var nextPage = function nextPage() {
      if (!isLastPage()) {
        //get first message item
        currentPage++;
        var startIndex = config.pageSize*(currentPage-1);
        var startKey = fireMessages.$keyAt(startIndex);
        //pageFirstKey = startKey;
        navRef = new Firebase(config.MSGFBURL).startAt(null, startKey).limitToFirst(config.pageSize);
        displayMessages = $firebaseArray(navRef);
      }
      return displayMessages;
    };

    var previousPage = function previousPage() {
      if (!isFirstPage()) {
        currentPage--;
        var startIndex = config.pageSize*(currentPage-1);
        var startKey = fireMessages.$keyAt(startIndex);
        navRef = new Firebase(config.MSGFBURL).startAt(null, startKey).limitToFirst(config.pageSize);
        displayMessages = $firebaseArray(navRef);
      }
      return displayMessages;
    };


    var showPage = function (){
      return currentPage;
    };

    var pageCount = function (){
      var df = $q.defer();
      fireMessages.$loaded().then(function () {
        itemCount = fireMessages.length;
        totalPage = Math.ceil(itemCount / config.pageSize);
        df.resolve(totalPage);
      });
      return df.promise;
    };

    return {
      add: add,
      remove: remove,
      update: update,
      nextPage: nextPage,
      previousPage: previousPage,
      getMessage: getMessage,
      getFirstPageMessages: getFirstPageMessages,
      isLastPage: isLastPage,
      isFirstPage: isFirstPage,
      currentPage:showPage,
      totalPage: pageCount,
      getFbMessages: getFbMessages
    };
  });
