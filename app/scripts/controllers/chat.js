'use strict';
(function(angular){
  angular.module('firebaseApp')
    .controller('ChatCtrl', function ($scope, config, $window, $rootScope, MessageService) {

      //all changes(add, update, remove) is update both ways
      setMenu();
      $scope.messages = MessageService.getFbMessages();
      console.log($scope.user);
      $scope.sendMessage = function () {
        var session = $scope.user;
        var newMessage = {
          user: session.password.email,
          text: $scope.currentText
        };
        var promise = MessageService.add(newMessage);
        promise.then(function (data) {
          console.log(data);
        });
        $scope.currentText = '';
      };

      function setMenu() {
        if ($rootScope.user == null) {
          $rootScope.login = {text: " Login", link: "/#/login", icon: "glyphicon glyphicon-log-in"};
          $rootScope.welcome = {text: " Register", link: "/#/register", icon: "glyphicon glyphicon-user"};
          $rootScope.loggedIn = false;
        } else {
          $rootScope.login = {text: " Logout", link: "/#/logout", icon: "glyphicon glyphicon-log-out"};
          $rootScope.welcome = {text: "Welcome " + $rootScope.user.password.email, link: "", icon: ""};
          $rootScope.loggedIn = true;
        }
      }
    });
}(window.angular));
