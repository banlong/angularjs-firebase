'use strict';
angular.module('firebaseApp')
  .controller('LogoutCtrl', function ($window, config, $rootScope, $scope) {
    var fbRef = new Firebase(config.FBURL);
    $rootScope.login = {text: " Login", link: "/#/login", icon: "glyphicon glyphicon-log-in"};
    $rootScope.welcome = {text: " Register", link: "/#/register", icon: "glyphicon glyphicon-user"};
    $rootScope.loggedIn = false;

    fbRef.onAuth(authDataCallback);

    fbRef.unauth(function () {
      $window.location.href = '/#/home';
    });

    //TODO: the session is till remain after unauth called

    // Create a callback which logs the current auth state
    function authDataCallback(authData) {
      $rootScope.user = authData;
      if (authData) {
        console.log("User " + authData.uid + " is logged in with " + authData.provider);
      } else {
        console.log("User is logged out");
      }
    }


  });
