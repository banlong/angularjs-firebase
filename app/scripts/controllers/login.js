'use strict';
(function (angular) {
  angular.module('firebaseApp')
    .controller('LoginCtrl', function ($scope, config, $window, $rootScope) {
      var fbRef = new Firebase(config.FBURL);
      $scope.errors = [];
      $scope.user = {
        email: '',
        password: ''
      };

      $scope.login = function () {
        var errors = [];

        if ($scope.user.email == '') {
          errors.push('Please enter an email');
        }

        if ($scope.user.password == '') {
          errors.push('Password must not be blank');
        }

        if (errors.length > 0) {
          $scope.errors = errors;
          return;
        }

        fbRef.authWithPassword({
          email: $scope.user.email,
          password: $scope.user.password
        }, function (error, authData) {
          if (error) {
            errors.push("Login Failed! " + error.message);
            $scope.errors = errors
          } else {
            $rootScope.user = authData;
            console.log("Authenticated successfully with payload:", authData);
            console.log("Uid:", authData.uid);
            console.log("Token:", authData.token);
            $rootScope.login = {text: " Logout", link: "/#/logout", icon: "glyphicon glyphicon-log-out"};
            $rootScope.welcome = {text: "Welcome " + $rootScope.user.password.email, link: "", icon: ""};
            $rootScope.loggedIn = true;
            $window.location.href = "/#/chat";
          }
        });
      };
      $rootScope.logout = function () {
        //$rootScope.login = {text: " Login", link: "/#/login", icon: "glyphicon glyphicon-log-in"};
        //$rootScope.welcome = {text: " Register", link: "/#/register", icon: "glyphicon glyphicon-user"};
        //$rootScope.loggedIn = false;
        fbRef.unauth(function () {
          $window.location.href = '/#/home';
        });
        $rootScope.user = null;
        $rootScope.setMenu;
      }
    });
}(window.angular));
