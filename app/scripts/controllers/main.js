/"global Firebase"/;
'use strict';
angular.module('firebaseApp')
  .controller('MainCtrl', function(config, $scope, MessageService, $rootScope){
    $scope.currentText = null;
    $scope.currentUser = null;
    $scope.currentKey = null;
    $scope.targetMsg  = null;

    //Create synchronized messages with firebase data
    //all changes(add, update, remove) is update both ways
    setMenu();
    $scope.messages = MessageService.getFirstPageMessages();
    getPageInfo();


    //fill data into textbox in the model
    $scope.getTagetMsg =  function(message){
      //data to restore the previous status if choose cancel button
      $scope.currentUser = message.user;
      $scope.currentText = message.text;
      $scope.currentKey = message.$id;
      //target message
      $scope.targetMsg = message;

    };

    $scope.saveMessage = function(){
      MessageService.update($scope.targetMsg);
      $scope.targetMsg  = null;
      $scope.currentUser = null;
      $scope.currentText = null;
      $scope.currentKey = null;
    };

    $scope.cancelUpdate = function(){
      $scope.targetMsg.user = $scope.currentUser;
      $scope.targetMsg.text = $scope.currentText;
      $scope.currentText = null;
      $scope.currentUser = null;
    };

    $scope.addNewMessage = function(){
      var newMessage = {
        user: $scope.currentUser,
        text: $scope.currentText
      };
      var promise = MessageService.add(newMessage);
      promise.then(function(data){
        console.log(data);
      });
      $scope.currentText = null;
      $scope.currentUser = null;
      getPageInfo();
    };

    $scope.removeMessage = function(){
      $scope.messages = MessageService.remove($scope.targetMsg);
      $scope.targetMsg  = null;
      getPageInfo();
    };

    $scope.pageNext = function() {
      $scope.messages = MessageService.nextPage();
      getPageInfo();
    };

    $scope.pageBack = function(){
      $scope.messages = MessageService.previousPage();
      getPageInfo();
    };

    function enableButton() {
      if (MessageService.isFirstPage() && MessageService.isLastPage()) {
          $scope.isBackDisabled = true;
          $scope.isNextDisabled = true;
      } else if(MessageService.isFirstPage()) {
          $scope.isBackDisabled = true;
          $scope.isNextDisabled = false;
      } else if(MessageService.isLastPage()) {
          $scope.isNextDisabled = true;
          $scope.isBackDisabled = false;
      }else{
          $scope.isBackDisabled = false;
          $scope.isNextDisabled = false;
      }
    }

    function getPageInfo(){
      $scope.currentPage = MessageService.currentPage();
      MessageService.totalPage().then(function(data){
        $scope.totalPage = data;
        enableButton();
      });
    }

    function setMenu(){
      if($rootScope.user == null){
        $rootScope.login = {text: " Login", link : "/#/login", icon : "glyphicon glyphicon-log-in"};
        $rootScope.welcome = {text: " Register", link : "/#/register", icon : "glyphicon glyphicon-user"};
        $rootScope.loggedIn = false;
      }else{
        $rootScope.login = {text: " Logout", link : "/#/logout", icon : "glyphicon glyphicon-log-out"};
        $rootScope.welcome = {text: "Welcome " + $rootScope.user.password.email, link : "", icon : ""};
        $rootScope.loggedIn = true;
      }
    }

  });
