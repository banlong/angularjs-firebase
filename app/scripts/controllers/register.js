'use strict';
angular.module('firebaseApp')
  .controller('RegisterCtrl', function ($scope, config, $window) {
    var fbRef = new Firebase(config.FBURL);
    $scope.errors = [];
    $scope.registeredUser = {
      email: '',
      password:'',
      confirmPassword:''
    };
    $scope.register = function register(){
      var errors = [],
          user = $scope.registeredUser;
      if(user.email==''){
        errors.push('Please enter an email');
      }

      if(user.password == ''){
        errors.push('Password must not be blank');
      } else if(user.password !== user.confirmPassword){
        errors.push('Passwords must match');
      }

      if(errors.length > 0){
        $scope.errors = errors;
        return;
      }


      fbRef.createUser({
        email: user.email,
        password: user.password
      }, function(error, userData) {
        if (error) {
          switch (error.code) {
            case "EMAIL_TAKEN":
              errors.push("The new user account cannot be created because the email is already in use.");
              break;
            case "INVALID_EMAIL":
              errors.push("The specified email is not a valid email.");
              break;
            default:
              errors.push("Error creating user:" + error);
          }
          $scope.errors = errors;
        } else {
          console.log("Successfully created user account with uid:", userData.uid);
          $window.location.href='/#/home';
        }
      });
    }
  });
